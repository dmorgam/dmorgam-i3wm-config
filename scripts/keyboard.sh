#!/bin/sh

KEYB=$(setxkbmap -query | head -n 3 | tail -n 1 | cut -b 13-14 | tr '[:lower:]' '[:upper:]')

if [ $KEYB = US ]
then
  setxkbmap es
else
  setxkbmap us
fi

KEYB=$(setxkbmap -query | head -n 3 | tail -n 1 | cut -b 13-14 | tr '[:lower:]' '[:upper:]')

notify-send "Keyboard "$KEYB
