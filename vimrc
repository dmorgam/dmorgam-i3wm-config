" Configuracion de vim


" Show line numbers
set number

" Show file stats
set ruler

" Tabulador como espacios
set expandtab
set tabstop=3

set visualbell
set encoding=utf-8
set t_Co=256

" Last line
set showmode
set showcmd

let g:airline_theme='onedark'

set bg=dark
colo gruvbox

syntax on

let g:NERDTreeNodeDelimiter = "\u00a0"
